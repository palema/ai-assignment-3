argmax(A::AbstractArray; dims=:) = findmax(A; dims=dims)[2]

#Struct  MDP
struct MDP
    initial
    states
    actions
    terminal_states
    transitions
    gamma
    rewards 
    #The constructor of MDP struct
    function MDP(initial,actions, terminal_states , transitions ,states, discount ,reward)
        return new(initial, states, actions, terminal_states, transitions, discount,reward);
    end
end

#Reward function for returning the reward
function  reward(mdp , state)
    print("\nrewards :",mdp.rewards,"\n")
    print("\nReward and state :",state ,"\n")
    print("\nReward  :",mdp.rewards[state] , "\n")
    return mdp.rewards[state];
end


#transion model to return the transition_model of the state with the given action
function transition_model(mdp, state ,action)
    print("\nState  :",mdp.transitions[state] ,'\n')
    print("\nstate-action :",mdp.transitions[state][action],'\n')
    return mdp.transitions[state][action];
end


#actions function to return the actions
function actions(mdp , state)
    if(state in mdp.terminal_states)
        return Set{Nothing}([nothing])
    else
        print("\nActions :",mdp.actions)
        return mdp.actions;
        
    end 
end



#value iteration to return the utilities values of state after conveging 
function value_iteration(mdp , epsilon = 0.001)
    local U_prime::Dict = Dict(collect(Pair(state, 0.0) for state in mdp.states));
    while (true)
        local U::Dict = copy(U_prime);
        print("The states :",U_prime)
        local delta::Float64 = 0.0;
        for state in mdp.states
            U_prime[state] = (reward(mdp, state)
                                + (mdp.gamma
                                * max((sum(collect(p * U[state_prime] 
                                                    for (p, state_prime) in transition_model(mdp, state, action)))
                                        for action in actions(mdp, state))...)));
            delta = max(delta, abs(U_prime[state] - U[state]));
        end
        if (delta < ((epsilon * (1 - mdp.gamma))/mdp.gamma))
            return U;
        end
    end
end



# expected utility function returns the expected utility of doing action in state s
function expected_utility(mdp , U , state , action)
    return sum((p * U[state_prime] for (p, state_prime) in transition_model(mdp, state, action)));
end



# optimal_policy function returns the optimal_policy 
function optimal_policy(mdp , U)
    local pi::Dict = Dict();
    for state in mdp.states
        pi[state] = argmax(collect(actions(mdp, state)),(function(action)
        return expected_utility(mdp, U, state, action);
        end));
    end
    return pi;
end


#Reading information about the model
println("\nEnter discount factor :")
dicount = readline()

println("Reading the rewards \n") 
println("Enter reward of S0 :")
r0 = readline()

println("Enter reward of S1 :")
r1 = readline()

println("Enter reward of S2 :")
r2 = readline()

println("Enter reward of S3 :")
r3 = readline()

println("\nReading the transition_model \n")
println("In state 0 \n")
println("Reading a1b \n")

println("\nEnter (p0 , S0) :")
p0 = readline()

println("\nEnter (p1 , S1) :")
p1 = readline()

println("\nIn state 1")
println("\nReading a1c (3) \n")
println("\nEnter (p10, S1 ):")
p10 = readline()


println("\nEnter (p1 , S4) :")
p11 = readline()


println("\nEnter (p12 , S2) :")
p12 = readline()


println("\nReading a2c (2) \n")
println("Enter (p21 , S1) :")
p21= readline()


println("\nEnter (p22 , S2) :")
p22 = readline()

println("\nIn state 2 ")
println("\nReading a1d (3)\n")
println("\nEnter (p30 , S2 ):")
p30 = readline()

println("\nEnter (p31 , S5) :")
p31 = readline()


println("\nEnter (p32 , S3) :")
p32 = readline()

println("\nReading a2d (2) \n")
println("\nEnter (p40 , S2) :")
p40= readline()


println("\nEnter (p41 , S3) :")
p41 = readline()




#transition model for our system
transitionModel = Dict([
    Pair("S0", Dict([                                                                   #state S0
        Pair("a0b", (1, "S0")),                                                         #action a0b (S0)
        Pair("a1b", (p0,"S0")) , Pair("a1b",(p1,"S1"))])),                           #action a1b (S0)
    Pair("S1", Dict([                                                                   #State S1
        Pair("a0c", (1, "S1")),                                                         #action a0c (S1)
        Pair("a1c", (p10, "S1")), Pair("a1c",(p11 ,"S4")),Pair("a1c",(p12,"S2")),       #action a1c (S1)
        Pair("a2c", (p21 ,"S1")), Pair("a2c" ,(p22,"S2"))])),                           #action a2c (S1)
    Pair("S2", Dict([                                                                   #State S2 
        Pair("a0d", (1, "S2")),                                                         #action a0d  (S2)
        Pair("a1d", (p30, "S2")),Pair("a1d" ,(p31 ,"S5")), Pair("a1d" ,(p32 , "S3")),   #action a1d  (S2)
        Pair("a2d", (p40 ,"S2")),Pair("a2d",(p41 , "S3"))])),                           #action a2d  (S2)
        ]);

#nDictionary for our states rewards
rewards = Dict("S0" => r0 , "S1" => r1, "S2" => r2 , "S3" => r3)

mdp = MDP("S0", Set(["a0b","a1b","a0c","a1c","a2c","a0d","a1d","a1d","a2d"]), Set(["S3"]), transitionModel, Set(["S0","S1","S2","S3"]),dicount,rewards);


#calling the transition_model
transition= transition_model(mdp, "S0", "a0b")
print("Initial State ",transition ,'\n')
value = value_iteration(mdp)
print("Result of value iteration :" ,value)

optimal = optimal_policy(mdp,value)
print(optimal)
