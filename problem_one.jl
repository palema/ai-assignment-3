function pretty_print(d::Dict, pre=1)
    for (k,v) in d
        if typeof(v) <: Dict
            s = "$(repr(k)) => "
            println(join(fill(" ", pre)) * s)
            pretty_print(v, pre+1+length(s))
        else
            println(join(fill(" ", pre)) * "$(repr(k)) => $(repr(v))")
        end
    end
    nothing
end

struct Constraint
    variables::Vector{String}
    place1::String
    place2::String
    function Constraint(variables::Vector{String}, place1, place2)
        return new(variables, place1, place2)
    end
end

function satisfied(constraint::Constraint, assignment)::Bool
    if(!haskey(assignment, constraint.place1) || !haskey(assignment, constraint.place2))
        return true
    end
    return assignment[constraint.place1] != assignment[constraint.place2]
end

struct CSP
    variables::Vector
    domains::Dict{String, Vector}
    constraints::Dict{String, Vector{}}

    function CSP(vars, doms, cons=Dict())
        variables = vars
        domains = doms
        constraints = cons
        for var in vars
            constraints[var] = Vector()
            if (!haskey(domains, var))
                error("Every variable should have a domain assigned to it.")
            end
        end
        return new(variables, domains, constraints)
    end
end

function add_constraint(csp:: CSP, constraint::Constraint)
    for vari in constraint.variables
        if (!(vari in csp.variables))
            error("Variable in constraint not in CSP")
        else
            push!(csp.constraints[vari], constraint)
        end
    end
end

function consistent(csp:: CSP, variable, assignment)::Bool
    for constraint in csp.constraints[variable]
        if (!satisfied(constraint, assignment))
            return false
        end
        return true
    end
end

function backtracking_search(csp:: CSP,  assignment=Dict(), path=Dict())::Union{Dict,Nothing}
     # assignment is complete if every variable is assigned (our base case)
     if length(assignment) == length(csp.variables)
        return assignment
     end

    # get all variables in the CSP but not in the assignment
    unassigned::Vector{String} = []
    for v in csp.variables
        if (!haskey(assignment, v))
            push!(unassigned, v)
        end
    end

    # get the every possible domain value of the first unassigned variable
    first = unassigned[1]
    # println(first)
    # pretty_print(csp.domains)
    for value in csp.domains[first]
        local_assignment = deepcopy(assignment)
        local_assignment[first] = value
        # if we're still consistent, we recurse (continue)
        if consistent(csp, first, local_assignment)
            # forward checking, prune future assignments that will be inconsistent
            for un in unassigned
                ass = deepcopy(local_assignment)
                for (i, val) in enumerate(csp.domains[un])
                    ass[un] = val
                    if un != first
                        if(!consistent(csp, un, ass))
                            deleteat!(csp.domains[un], i)
                        end
                    end
                end
            end
            path[first] = csp.domains
            # println("reduced")
            # pretty_print(csp.domains)
            result = backtracking_search(csp, local_assignment)
            #backtrack if nothing is found
            if result !== nothing
                pretty_print(path)
                return result
            end
        end
    end
    return nothing
end

variables = ["Western Australia", "Northern Territory", "South Australia", "Queensland", "New South Wales", "Victoria", "Tasmania"]

domains = Dict()
for variable in variables
    domains[variable] = ["red", "green", "blue"]
end

game = CSP(variables, domains)
add_constraint(game, Constraint(["Western Australia", "Northern Territory"], "Western Australia", "Northern Territory"))
add_constraint(game, Constraint(["Western Australia", "South Australia"], "Western Australia", "South Australia"))
add_constraint(game, Constraint(["South Australia", "Northern Territory"], "South Australia", "Northern Territory"))
add_constraint(game, Constraint(["Queensland", "Northern Territory"], "Queensland", "Northern Territory"))
add_constraint(game, Constraint(["Queensland", "South Australia"],"Queensland", "South Australia"))
add_constraint(game, Constraint(["Queensland", "New South Wales"], "Queensland", "New South Wales"))
add_constraint(game, Constraint(["New South Wales", "South Australia"], "New South Wales", "South Australia"))
add_constraint(game, Constraint(["Victoria", "South Australia"], "Victoria", "South Australia"))
add_constraint(game, Constraint(["Victoria", "New South Wales"], "Victoria", "New South Wales"))
add_constraint(game, Constraint(["Victoria", "Tasmania"], "Victoria", "Tasmania"))

solution = backtracking_search(game)
if solution != Nothing
    print("solution is: ", solution)
else
    println("There's no solution")
end

