# Function for taking in User input into the Game
# The Game takes in 8 values which form the bases of the 4 choices for the Players 1 and 2

function p_Input()
    println("Enter 8 Values:")
    v1 = readline()
    v2 = readline()
    v3 = readline()
    v4 = readline()
    v5 = readline()
    v6 = readline()
    v7 = readline()
    v8 = readline()
    
    # Take user input and store it in a tuple Array
    p_values = [(v1, v2) (v3, v4); (v5, v6) (v7, v8)]
    
    println("You have entered the following value choices")
    println(p_values)
    
    return p_values
end


# Create an object of the tuple array to be used in the preceeding functions
# The values of the Game input by the user are stored in the p_values object
p_values = p_Input()


# This function determines the striclty dominant strategy for player 1. 
# It takes in value choices for Player 1

function p1_dominant(p)
    a = [(p[1]) (p[2])]
    b = [(p[3]) (p[4])]
    
    a1 = a[1]
    a2 = a[2]

    b1 = b[1]
    b2 = b[2]
    
    # 1. Get all values
    
    val1 = parse(Int64, a1[1])
    val2 = parse(Int64, a1[2])
    
    val3 = parse(Int64, b1[1])
    val4 = parse(Int64, b1[2])
    
    val5 = parse(Int64, a2[1])
    val6 = parse(Int64, a2[2])
    
    val7 = parse(Int64, b2[1])
    val8 = parse(Int64, b2[2])
    
    # println("val1: ",val1)
    # println("val2: ",val2)
    # println("val3: ",val3)
    # println("val4: ",val4)
    # println("val5: ",val5)
    # println("val6: ",val6)
    # println("val7: ",val7)
    # println("val8: ",val8)
    
    # Dominant Strategy
    ds1 = val1 + val3
    ds2 = val5 + val7
    
    if ds1 > ds2
        println("Strategy a is the dominant strategy for Player 1")
        println("")
        println("Strategy b is the dominated strategy for Player 1")
    elseif ds2 > ds1
        println("Strategy b is the dominant strategy for Player 1")
        println("")
        println("Strategy a is the dominated strategy for Player 1")
    elseif ds1 == ds2
        println("There is a state of equilibrium between strategy a and strategy b, hence both strategies are dominant")
    else
        println("Player 1 has no dominant strategy!!!")
    end
        println("")
        
    # Strictly and Weakly dominant strategy
    a1 = val1 - val2
    a2 = val3 - val4
    
    sa1 = [(val1, val2)]
    sa2 = [(val3, val4)]   
        
    b1 = val5 - val6
    b2 = val7 - val8
    
    sa3 = [(val5, val6)]
    sa4 = [(val7, val8)]
    
    if ds1 > ds2 
        if a1 > a2
            println("Solution a. ", sa1 ," is the strictly dominant strategy for Player 1")
                println("")
            println("Solution a. ", sa2 ," is the weakly dominant strategy for Player 1")
        elseif a2 > a1
            println("Solution a. ", sa2 ," is the strictly dominant strategy for Player 1")
                println("")
            println("Solution a. ", sa1 ," is the weakly dominant strategy for Player 1")
        else
            println("There is a state of equilibrium between strategy a.", sa1 , " and strategy a.", sa2 ,"!!!")
        end
    elseif ds2 > ds1
        if b1 > b2
            println("Solution a. ", sa3 ," is the strictly dominant strategy for Player 1")
                println("")
            println("Solution a. ", sa4 ," is the weakly dominant strategy for Player 1")
        elseif b2 > b1
            println("Solution b. ", sa4 ," is the strictly dominant strategy for Player 1")
                println("")
            println("Solution b. ", sa3 ," is the weakly dominant strategy for Player 1")
        else
            println("There is a state of equilibrium between strategy b.", sa3 , " and strategy b.", sa4 ,"!!!")
        end
    end

    println("")
    
end

# Strictly dominant strategy for Player 2
function p2_dominant(p)
    a = [(p[1]) (p[3])]
    b = [(p[2]) (p[4])]
    
    a1 = a[1]
    a2 = a[2]
    
    b1 = b[1]
    b2 = b[2]
    
    # 1. Get all values
    
    val1 = parse(Int64, a1[1])
    val2 = parse(Int64, a1[2])
    
    val3 = parse(Int64, b1[1])
    val4 = parse(Int64, b1[2])
    
    val5 = parse(Int64, a2[1])
    val6 = parse(Int64, a2[2])
    
    val7 = parse(Int64, b2[1])
    val8 = parse(Int64, b2[2])
    
    # Dominant Strategy
    ds1 = val2 + val4
    ds2 = val5 + val8
    
    if ds1 > ds2
        println("Strategy c is the dominant strategy for Player 2")
        println("")
        println("Strategy d is the dominated strategy for Player 2")
    elseif ds2 > ds1
        println("Strategy d is the dominant strategy for Player 2")
        println("")
        println("Strategy c is the dominated strategy for Player 2")
    elseif ds1 == ds2
        println("There is a state of equilibrium between strategy c and strategy d, hence both strategies are dominant")
    else
        println("Player 2 has no dominant strategy!!!")
    end
        println("")
        
    # Strictly and Weakly dominant strategy
    a1 = val2 - val1
    a2 = val4 - val3
    
    sa1 = [(val2, val4)]
    sa2 = [(val6, val8)]   
        
    b1 = val6 - val5
    b2 = val8 - val7
    
    sa3 = [(val5, val6)]
    sa4 = [(val7, val8)]
    
    if ds1 > ds2 
        if a1 > a2
            println("Solution c. ", sa1 ," is the strictly dominant strategy for Player 2")
                println("")
            println("Solution c. ", sa2 ," is the weakly dominant strategy for Player 2")
        elseif a2 > a1
            println("Solution c. ", sa2 ," is the strictly dominant strategy for Player 2")
                println("")
            println("Solution c. ", sa1 ," is the weakly dominant strategy for Player 2")
        else
            println("There is a state of equilibrium between strategy c.", sa1 , " and strategy c.", sa2 ,"!!!")
        end
    elseif ds2 > ds1
        if b1 > b2
            println("Solution d. ", sa3 ," is the strictly dominant strategy for Player 2")
                println("")
            println("Solution d. ", sa4 ," is the weakly dominant strategy for Player 2")
        elseif b2 > b1
            println("Solution d. ", sa4 ," is the strictly dominant strategy for Player 2")
                println("")
            println("Solution d. ", sa3 ," is the weakly dominant strategy for Player 2")
        else
            println("There is a state of equilibrium between strategy d.", sa3 , " and strategy d.", sa4 ,"!!!")
        end
    end

    println("")
    
end

# The functions p1_toInt and p2_toInt simply convert the input values by the user to Integers
function p1_toInt(p)
    c = [(p[1]) (p[3])];
    d = [(p[2]) (p[4])];
    
    c1 = c[1];
    c2 = c[2];
     
    d1 = d[1];
    d2 = d[2];
  
    val1 = parse(Int64, c1[1]);
    val2 = parse(Int64, c1[2]);
    val3 = parse(Int64, c2[1]);
    val4 = parse(Int64, c2[2]);
    val5 = parse(Int64, d1[1]);
    val6 = parse(Int64, d1[2]);
    val7 = parse(Int64, d2[1]);
    val8 = parse(Int64, d2[2]);
    
    values = [(val1, val2) (val3, val4); (val5, val6) (val7, val8)];
    return values
end

function p2_toInt(p)
    c = [(p[1]) (p[2])];
    d = [(p[3]) (p[4])];
    
    c1 = c[1];
    c2 = c[2];
     
    d1 = d[1];
    d2 = d[2];
  
    val1 = parse(Int64, c1[1]);
    val2 = parse(Int64, c1[2]);
    val3 = parse(Int64, c2[1]);
    val4 = parse(Int64, c2[2]);
    val5 = parse(Int64, d1[1]);
    val6 = parse(Int64, d1[2]);
    val7 = parse(Int64, d2[1]);
    val8 = parse(Int64, d2[2]);
    
    values = [(val1, val2) (val3, val4); (val5, val6) (val7, val8)]
    return values
end


# This function is the entry point for Player 1
function Player1(p)
    
    v = p1_toInt(p)
    
    a = [(v[1]) (v[3])]
    b = [(v[2]) (v[4])]
    
    println("")
    println("******************** Player 1 ********************")
    println("")
    println("a. ",a)
    println("b. ",b)
    println("")
    p1_dominant(p)
    println("")
end

# This function is the entry point for Player 2
function Player2(p)
    
    v = p2_toInt(p)
    
    c = [(v[1]) (v[3])]
    d = [(v[2]) (v[4])]
    
    println("")
    println("******************** Player 2 ********************")
    println("")
    println("c. ",c)
    println("d. ",d)
    println("")
    p2_dominant(p)
    
end


# Game_Theory is the main function of The simultaneous Game 
function Game_Theory(p)
    Player1(p)
        
    println("")
        
    Player2(p)
end    

Game_Theory(p_values)